
const { graphql }  = require('graphql')
const readline  = require('readline')
const mySchema = require('./schema/main')
const  MongoClient  =  require('mongodb')
const assert = require('assert')
const graphqlHTTP = require('express-graphql')
const express = require('express')

const app = express();

const MONGO_URL = 'mongodb://localhost:27017/test'

const rli = readline.createInterface({
    input: process.stdin,
    output: process.stdout
})

MongoClient.connect(MONGO_URL, async (err,db) => {
    app.use('/graphql',graphqlHTTP({
        schema: mySchema,
        context: { db },
        graphiql: true
    }))
});

app.listen(3000, () => console.log('Running Express.js on port 3000'))



